��    "      ,  /   <      �      �          !  	   *     4  D   9     ~     �  #   �     �     �  	   �     �     �     �     �                  >   '     f     m  	   {     �     �     �  r   �  !   <  '   ^     �  +   �  !   �     �  �  �      �     �     �     �     �  S   �     7     ?  9   D     ~     �     �     �     �     �     �     �     	  	   *	  U   4	     �	     �	  	   �	     �	     �	     �	  �   �	     o
  *   �
     �
  /   �
  #   �
                                              	   
                                                                                       !      "                     
        Transaction %(id)s
     Amount Currency Dashboard Date Incorrect signature. Check SystemPayTransaction #%s for more details Message Mode No transactions have been made yet. Op. Type Operation type Order N° Order preview Payment Place order Preview order Request params Response params Result Signature not valid. Get '{signature}' instead of '{expected}' Status Submitting... SystemPay SystemPay Transactions SystemPay transaction SystemPay transactions The data received are not complete: {printable_form_errors}. See the transaction record #{txn_id} for more details The transaction has been canceled The transaction is not verified for now Trans ID Unknown operation type '%(operation_type)s' You're going to be redirect to %s Your basket is empty Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-01-24 16:41+0100
PO-Revision-Date: 2012-11-27 15:29
Last-Translator: Loic Gallopin <lgallopin@gmail.com>
Language-Team: fr <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 0.9.6
Plural-Forms: nplurals=2; plural=(n > 1)
X-Translated-Using: django-rosetta 0.6.8
 
        Transaction %(id)s
     Montant Devise Tableau de bord Date Signature incorrecte. Vérifiez la transaction SystemPay #%s pour plus de détails. Message Mode Aucune transaction n'a été enregistrée pour l'instant. Type d'opération Type d'opération Commande N° Aperçu de la commande Paiement Valider la commande Aperçu de la commande Paramètres de requête Paramètres de réponse Résultat La signature n'est pas valide. Nous avons reçu '{signature}' au lieu de '{expected}' État Envoi... SystemPay Transaction SystemPay Transaction SystemPay Transactions SystemPay Les données reçues ne sont pas complètes : {printable_form_errors}. Merci de consulter la transation #{txn_id} pour plus de détails. La transaction a été annulée La transaction n'a pas pu être vérifiée Trans ID Type d'opération inconnue '%(operation_type)s' Vous allez être redirigés vers %s Votre panier est vide 