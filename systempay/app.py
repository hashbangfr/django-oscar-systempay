from django.urls import path
from django.views.decorators.csrf import csrf_exempt
from oscar.core.application import Application

from . import views


class SystemPayApplication(Application):
    name = 'systempay'

    secure_redirect_view = views.SecureRedirectView
    return_response_view = views.ReturnResponseView
    cancel_response_view = views.CancelResponseView
    handle_ipn_view = views.IpnView

    def __init__(self, *args, **kwargs):
        super(SystemPayApplication, self).__init__(*args, **kwargs)

    def get_urls(self):
        urlpatterns = super(SystemPayApplication, self).get_urls()
        urlpatterns += [
            path(
                'secure-redirect',
                self.secure_redirect_view.as_view(),
                name='secure-redirect'
            ),
            path(
                'return',
                self.return_response_view.as_view(),
                name='return-response'
            ),
            path(
                'cancel',
                self.cancel_response_view.as_view(),
                name='cancel-response'
            ),
            path(
                'handle-ipn',
                csrf_exempt(self.handle_ipn_view.as_view()),
                name='handle-ipn'
            ),
        ]
        return self.post_process_urls(urlpatterns)


application = SystemPayApplication()
