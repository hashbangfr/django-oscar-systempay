from django.urls import path

from .app import application as oscar_app

urlpatterns = [
    path('', oscar_app.urls),
]
