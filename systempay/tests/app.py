from oscar.app import Shop as CoreShop
from oscar.core.loading import get_class
from django.urls import path


class Shop(CoreShop):
    systempay_app = get_class('systempay.app', 'application')
    systempay_dashboard = get_class('systempay.dashboard.app', 'application')

    def get_urls(self):
        urls = super().get_urls()
        urls += [
            path('checkout/systempay/', self.systempay_app.urls),
            path('dashboard/systempay', self.systempay_dashboard.urls),
        ]
        return urls


application = Shop()
