import pytest

from django.urls import reverse

from oscar.test.factories import create_basket, create_order

from systempay.models import SystemPayTransaction


pytestmark = pytest.mark.django_db


response_txn_data = {
    "mode": "RESPONSE",
    "operation_type": "DEBIT",
    "trans_id": "593316",
    "trans_date": "20190304162847",
    "order_number": "100007",
    "amount": "70.00",
    "auth_result": "00",
    "result": "00",
    "error_message": None,
    "raw_request": (
        "vads_order_id=100007&vads_cust_name=&"
        "vads_cust_email=user%40exemple.net&vads_cust_id=1&"
        "vads_ship_to_name=firstname+lastname&"
        "vads_ship_to_street=42+rue+line1&vads_ship_to_street2=&"
        "vads_ship_to_city=city&vads_ship_to_state=&vads_ship_to_zip=01234&"
        "vads_ship_to_country=fr&vads_action_mode=interactive&"
        "vads_amount=7000&vads_currency=978&vads_ctx_mode=test&"
        "vads_page_action=payment&vads_payment_config=single&"
        "vads_site_id=12345678&vads_trans_date=20190304162847&"
        "vads_trans_id=593316&vads_validation_mode=&vads_version=v2&"
        "vads_return_mode=get&"
        "vads_url_success=http%3a%2f%2flocalhost%3a8000%2fcheckout"
        "%2fsystempay%2freturn&vads_url_return=http%3a%2f%2flocalhost%3a8000"
        "%2fcheckout%2fsystempay%2freturn&vads_url_cancel=http%3a%2f%2f"
        "localhost%3a8000%2fcheckout%2fsystempay%2fcancel&"
        "vads_url_refused=http%3a%2f%2flocalhost%3a8000%2fcheckout%2f"
        "systempay%2fcancel&vads_redirect_success_timeout=5&"
        "vads_redirect_success_message=you%27re+going+to+be+redirect+to+"
        "oscar+shop&vads_redirect_error_timeout=5&"
        "signature=oedtpi%2bk2epxbkaq2insget0gg8zka9qd60fml66iye%3d"
    ),
    "date_created": "2019-03-04t16:29:04.475z",
    "session_key": ""
}


def test_return_view_to_preview(client):
    basket = create_basket()
    basket.freeze()

    session = client.session
    session['checkout_data'] = {
        'submission': {
            'basket_id': basket.id,
        }
    }
    session.save()

    response = client.get(reverse('systempay:return-response'))
    assert response.status_code == 302
    assert response['Location'] == reverse('checkout:preview')

    basket.refresh_from_db()
    assert basket.status == 'Open'


def test_return_view_to_thank_you(client):
    order = create_order()

    response_txn_data['order_number'] = order.number
    SystemPayTransaction.objects.create(**response_txn_data)

    session = client.session
    session['checkout_data'] = {
        'submission': {
            'order_number': order.number,
        }
    }
    session.save()

    response = client.get(reverse('systempay:return-response'))
    assert response.status_code == 302
    assert response['Location'] == reverse('checkout:thank-you')
    assert client.session['checkout_order_id'] == order.id
