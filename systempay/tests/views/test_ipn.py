import pytest

from importlib import import_module

from django.urls import reverse
from django.conf import settings
from django.core.management import call_command

from oscar.core.loading import get_model
from oscar.test.factories import create_basket, UserFactory

from systempay.models import SystemPayTransaction

pytestmark = pytest.mark.django_db

Order = get_model('order', 'Order')


post_data = {
    'vads_amount': '7000',
    'vads_auth_mode': 'FULL',
    'vads_auth_number': '3fdf88',
    'vads_auth_result': '00',
    'vads_capture_delay': '0',
    'vads_card_brand': 'CB',
    'vads_card_number': '497010XXXXXX0014',
    'vads_payment_certificate': '59654dd0e592e43d96f7cecc38a8793715ecb0f1',
    'vads_ctx_mode': 'TEST',
    'vads_currency': '978',
    'vads_effective_amount': '7000',
    'vads_effective_currency': '978',
    'vads_site_id': '38256069',
    'vads_trans_date': '20190304162847',
    'vads_trans_id': '593316',
    'vads_trans_uuid': '6d2eaf4e457b4b968d23f50b8310bfe9',
    'vads_validation_mode': '0',
    'vads_version': 'V2',
    'vads_warranty_result': 'YES',
    'vads_payment_src': 'EC',
    'vads_order_id': '100007',
    'vads_order_info': '',
    'vads_order_info2': '',
    'vads_order_info3': '',
    'vads_cust_email': 'user@exemple.net',
    'vads_cust_id': '1',
    'vads_cust_title': '',
    'vads_cust_country': '',
    'vads_contrib': '',
    'vads_user_info': '',
    'vads_cust_state': '',
    'vads_ship_to_name': 'firstname lastname',
    'vads_ship_to_street': '42 rue line1',
    'vads_ship_to_city': 'city',
    'vads_ship_to_zip': '01234',
    'vads_ship_to_country': 'FR',
    'vads_ship_to_last_name': 'firstname lastname',
    'vads_sequence_number': '1',
    'vads_contract_used': '12345678',
    'vads_trans_status': 'AUTHORISED',
    'vads_expiry_month': '6',
    'vads_expiry_year': '2020',
    'vads_bank_label': "Banque de démo et de l'innovation",
    'vads_bank_product': 'F',
    'vads_pays_ip': 'FR',
    'vads_presentation_date': '20190304162911',
    'vads_effective_creation_date': '20190304162911',
    'vads_operation_type': 'DEBIT',
    'vads_threeds_enrolled': 'Y',
    'vads_threeds_cavv': 'Q2F2dkNhdnZDYXZ2Q2F2dkNhdnY=',
    'vads_threeds_eci': '05',
    'vads_threeds_xid': 'cllVTTVRb1lNUDVrblFPc2E3YlE=',
    'vads_threeds_cavvAlgorithm': '2',
    'vads_threeds_status': 'Y',
    'vads_threeds_sign_valid': '1',
    'vads_threeds_error_code': '',
    'vads_threeds_exit_status': '10',
    'vads_result': '00',
    'vads_extra_result': '',
    'vads_card_country': 'FR',
    'vads_language': 'fr',
    'vads_brand_management': (
        '{"userChoice":false,"brandList":"CB|VISA","brand":"CB"}'
    ),
    'vads_hash': (
        '8d99a2847bbc4831470a8df7df2fc3262d01faed2632c076477f0a7d1a56e666'
    ),
    'vads_url_check_src': 'RETRY',
    'signature': 'Z5D3xrMIZ0+7/qTB13DyPN6rFZnseATQ3Su3q9Vubzk='
}

submit_txn_data = {
    "mode": "SUBMIT",
    "operation_type": None,
    "trans_id": "593316",
    "trans_date": "20190304162847",
    "order_number": "100007",
    "amount": "70.00",
    "auth_result": None,
    "result": None,
    "error_message": None,
    "raw_request": (
        "vads_order_id=100007&vads_cust_name=&"
        "vads_cust_email=user%40exemple.net&vads_cust_id=1&"
        "vads_ship_to_name=firstname+lastname&"
        "vads_ship_to_street=42+rue+line1&vads_ship_to_street2=&"
        "vads_ship_to_city=city&vads_ship_to_state=&vads_ship_to_zip=01234&"
        "vads_ship_to_country=fr&vads_action_mode=interactive&"
        "vads_amount=7000&vads_currency=978&vads_ctx_mode=test&"
        "vads_page_action=payment&vads_payment_config=single&"
        "vads_site_id=12345678&vads_trans_date=20190304162847&"
        "vads_trans_id=593316&vads_validation_mode=&vads_version=v2&"
        "vads_return_mode=get&"
        "vads_url_success=http%3a%2f%2flocalhost%3a8000%2fcheckout"
        "%2fsystempay%2freturn&vads_url_return=http%3a%2f%2flocalhost%3a8000"
        "%2fcheckout%2fsystempay%2freturn&vads_url_cancel=http%3a%2f%2f"
        "localhost%3a8000%2fcheckout%2fsystempay%2fcancel&"
        "vads_url_refused=http%3a%2f%2flocalhost%3a8000%2fcheckout%2f"
        "systempay%2fcancel&vads_redirect_success_timeout=5&"
        "vads_redirect_success_message=you%27re+going+to+be+redirect+to+"
        "oscar+shop&vads_redirect_error_timeout=5&"
        "signature=oedtpi%2bk2epxbkaq2insget0gg8zka9qd60fml66iye%3d"
    ),
    "date_created": "2019-03-04t16:29:04.475z",
    "session_key": ""
}

session_data = {
    '_auth_user_backend': 'oscar.apps.customer.auth_backends.EmailBackend',
    '_auth_user_hash': '',
    '_auth_user_id': '',
    'checkout_data': {
        'billing': {},
        'shipping': {
            'method_code': 'free-shipping',
            'new_address_fields': {
                'country_id': 'FR',
                'first_name': 'Firstname',
                'id': None,
                'last_name': 'Lastname',
                'line1': '42 rue line1',
                'line2': '',
                'line3': '',
                'line4': 'City',
                'notes': '',
                'phone_number': '+33 1 23 45 67 89',
                'postcode': '01234',
                'search_text': '',
                'state': '',
                'title': 'Mr'
            }
        },
        'submission': {
            'basket_id': 7,
            'order_number': 100007,
            'order_total': '70.00'
        }
    }
}


def test_ipn_ok(client):
    call_command('oscar_populate_countries', '--initial-only')

    basket = create_basket()
    basket.owner = UserFactory()
    basket.save()

    SessionStore = import_module(settings.SESSION_ENGINE).SessionStore
    session = SessionStore()
    session.update(session_data)
    session['checkout_data']['submission']['basket_id'] = basket.id
    session.create()

    submit_txn_data['session_key'] = session.session_key

    SystemPayTransaction.objects.create(**submit_txn_data)
    response = client.post(reverse('systempay:handle-ipn'), post_data)
    assert response.status_code == 200
    assert Order.objects.count() == 1
