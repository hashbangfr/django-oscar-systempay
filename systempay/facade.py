from urllib.parse import urlencode
import logging

from django.conf import settings
from django.http import QueryDict
from django.utils.translation import ugettext_lazy as _

from .gateway import Gateway
from .models import SystemPayTransaction
from systempay.forms import SystemPayNotificationForm
from .utils import printable_form_errors, get_amount_from_systempay
from .exceptions import SystemPayFormNotValid, SystemPayResultError


logger = logging.getLogger('systempay')


class Facade(object):
    """
    A bridge between oscar's objects and the core gateway object.
    """

    def __init__(self):
        self.gateway = Gateway(
            settings.SYSTEMPAY_SANDBOX_MODE,
            settings.SYSTEMPAY_SITE_ID,
            settings.SYSTEMPAY_CERTIFICATE,
            getattr(settings, 'SYSTEMPAY_ACTION_MODE', 'INTERACTIVE'),
        )
        self.currency = getattr(settings, 'SYSTEMPAY_CURRENCY', 978)  # 978
        # stands for EURO (ISO 639-1)

    def get_result(self, form):
        return form.data.get('vads_result')

    def get_extra_result(self, form):
        return form.data.get('vads_extra_result')

    def get_auth_result(self, form):
        return form.data.get('vads_auth_result')

    def set_submit_form(self, basket, order_number=None, order_total=None,
                        billing_address=None, shipping_address=None, **kwargs):
        """
        Pre-populate the submit form with order data

        :order: an oscar order instance
        :kwargs: additional data, check the fields of the `SystemPaySubmitForm`
        class to see all possible values.
        """
        params = dict()

        params['vads_order_id'] = order_number

        if basket.owner:
            params['vads_cust_name'] = basket.owner.get_full_name()
            params['vads_cust_email'] = basket.owner.email
            params['vads_cust_id'] = basket.owner.pk

        if billing_address:
            params['vads_cust_title'] = billing_address.title or ""
            params['vads_cust_address'] = billing_address.line1 or ""
            params['vads_cust_city'] = billing_address.city or ""
            params['vads_cust_state'] = billing_address.state or ""
            params['vads_cust_zip'] = billing_address.postcode or ""
            params['vads_cust_country'] = billing_address.country.\
                iso_3166_1_a2

        if shipping_address:
            params['vads_ship_to_name'] = shipping_address.salutation
            params['vads_ship_to_street'] = shipping_address.line1 or ""
            params['vads_ship_to_street2'] = shipping_address.line2 or ""
            params['vads_ship_to_city'] = shipping_address.city or ""
            params['vads_ship_to_state'] = shipping_address.state or ""
            params['vads_ship_to_zip'] = shipping_address.postcode or ""
            params['vads_ship_to_country'] = shipping_address.country.\
                iso_3166_1_a2

        params.update(kwargs)

        form = self.gateway.get_submit_form(order_total, **params)
        self.gateway.sign(form)
        return form

    def set_txn(self, request):
        """
        Set a transaction from an Instant Payment Notification (IPN).

        :param request: request from Ipn View
        :return: SystemPayTransaction object
        """

        form = SystemPayNotificationForm(request.POST)

        # create transaction
        order_number = request.POST.get('vads_order_id')
        amount = get_amount_from_systempay(
            request.POST.get('vads_amount', '0')
        )
        txn = self.save_txn_notification(order_number, amount, request)

        if not form.is_valid():
            msg = _(
                "The data received are not complete: {printable_form_errors}. "
            ).format(
                printable_form_errors=printable_form_errors(form),
            )
            raise SystemPayFormNotValid(msg)

        if not self.gateway.is_signature_valid(form):
            raise SystemPayFormNotValid(_(
                "Incorrect signature for transaction ; "
                "got '{signature}', expected '{expected}'").format(
                    signature=form.cleaned_data['signature'],
                    expected=self.gateway.compute_signature(form)
                )
            )

        if not txn.is_complete():
            raise SystemPayResultError(txn.result)

        logger.info("Transaction %s has been proceeded." % txn.id)

        return txn

    def save_submit_txn(self, order_number, amount, form, session_key):
        """
        Save submitted transaction into the database.
        """
        return self.save_txn(order_number, amount, form.data,
                             SystemPayTransaction.MODE_SUBMIT, session_key)

    def save_txn_notification(self, order_number, amount, request):
        """
        Save notification transaction into the database.
        """
        return self.save_txn(order_number, amount, request.POST.copy(),
                             SystemPayTransaction.MODE_RESPONSE)

    def save_txn(self, order_number, amount, data, mode, session_key=''):
        """
        Save the transaction into the database, submitted or received.
        """
        # convert the QueryDict into a dict in case of POST data
        d = {}
        if isinstance(data, QueryDict):
            for k in data:
                d[k] = data.get(k)
        else:
            d.update(data)

        return SystemPayTransaction.objects.create(
            mode=mode,
            operation_type=d.get('vads_operation_type'),
            trans_id=d.get('vads_trans_id'),
            trans_date=d.get('vads_trans_date'),
            order_number=order_number,
            amount=amount,
            auth_result=d.get('vads_auth_result'),
            result=d.get('vads_result'),
            raw_request=urlencode(d),
            session_key=session_key
        )
