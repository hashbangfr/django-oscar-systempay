# encoding: utf-8
from decimal import Decimal as D
from importlib import import_module
import logging

from django.shortcuts import get_object_or_404
from django.conf import settings
from django.apps import apps
from django.views import generic
from django.contrib import messages
from django.http import HttpResponse, Http404, HttpResponseBadRequest
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from django.test import RequestFactory
from django.contrib.auth.models import AnonymousUser

from oscar.core.loading import get_class, get_classes

from .models import SystemPayTransaction
from .facade import Facade
from .gateway import Gateway
from .exceptions import SystemPayError

logger = logging.getLogger('systempay')

Basket = apps.get_model('basket', 'Basket')
Order = apps.get_model('order', 'Order')
Source = apps.get_model('payment', 'Source')
SourceType = apps.get_model('payment', 'SourceType')

PaymentDetailsView, OrderPlacementMixin, CheckoutSessionMixin = get_classes(
    'checkout.views',
    ['PaymentDetailsView', 'OrderPlacementMixin', 'CheckoutSessionMixin'])
PaymentError, UnableToTakePayment = get_classes(
    'payment.exceptions',
    ['PaymentError', 'UnableToTakePayment'])

EventHandler = get_class('order.processing', 'EventHandler')
UnableToPlaceOrder = get_class('order.exceptions', 'UnableToPlaceOrder')
Selector = get_class('partner.strategy', 'Selector')
Applicator = get_class('offer.applicator', 'Applicator')
CheckoutSessionData = get_class('checkout.utils', 'CheckoutSessionData')

selector = Selector()


# AUTHORISED = 'AUTHORISED'
# CAPTURED = 'CAPTURED'
CANCELLED = 'CANCELLED'


class SecureRedirectView(CheckoutSessionMixin, generic.DetailView):
    """
    Simple Redirect Page initiating the transaction throughout
    a fulfilled form of the payment order send to the SystemPay
    checkout.
    """
    template_name = 'systempay/secure_redirect.html'
    context_object_name = 'basket'

    _form = None

    def get_object(self):
        basket = get_object_or_404(
            Basket, pk=self.checkout_session.get_submitted_basket_id()
        )
        if basket.is_empty:
            raise Http404(_("Your basket is empty"))
        return basket

    def get(self, *args, **kwargs):
        basket = self.get_object()

        shipping_address = self.get_shipping_address(basket)
        order_total = self.checkout_session.get_order_total()

        facade = Facade()
        self._form = facade.set_submit_form(
            basket,
            order_number=self.checkout_session.get_order_number(),
            order_total=order_total,
            shipping_address=shipping_address,
            billing_address=self.get_billing_address(shipping_address),
        )
        facade.save_submit_txn(
            self.checkout_session.get_order_number(),
            order_total,
            self._form,
            self.request.session.session_key
        )
        response = super(SecureRedirectView, self).get(*args, **kwargs)

        logger.info("Basket #%s redirected to systempay", basket.id)

        return response

    def get_context_data(self, **kwargs):
        ctx = super(SecureRedirectView, self).get_context_data(**kwargs)
        ctx['submit_form'] = self._form
        ctx['SYSTEMPAY_GATEWAY_URL'] = Gateway.URL
        return ctx


class ReturnResponseView(OrderPlacementMixin, generic.RedirectView):
    def get_redirect_url(self, **kwargs):
        order_number = self.checkout_session.get_order_number()

        # check if transaction exists
        txns = SystemPayTransaction.objects.filter(
            mode=SystemPayTransaction.MODE_RESPONSE,
            order_number=order_number,
            result='00',
            error_message=None
        )
        if not txns.exists():
            messages.error(
                self.request, _("The transaction is not verified for now")
            )
            self.restore_frozen_basket()
            return reverse('checkout:preview')

        order = Order.objects.get(number=order_number)

        # Flush all session data
        self.checkout_session.flush()

        # Save order id in session so thank-you page can load it
        self.request.session['checkout_order_id'] = order.id

        return self.get_success_url()


class CancelResponseView(OrderPlacementMixin, generic.RedirectView):
    def get_redirect_url(self, **kwargs):
        self.restore_frozen_basket()
        messages.warning(self.request, _("The transaction has been canceled"))
        return reverse('checkout:preview')

    def restore_frozen_basket(self):
        try:
            fzn_basket = self.get_submitted_basket()
        except Basket.DoesNotExist:
            pass
        else:
            fzn_basket.thaw()
            fzn_basket.refresh_from_db()
            if self.request.basket.id != fzn_basket.id:
                self.request.basket.merge(fzn_basket)


class IpnView(OrderPlacementMixin, generic.View):
    """
    View to receive the Instant Payment Notification (IPN) from SystemPay
    checkout. Unfortunately, SystemPay doesn't provide a full batch
    services. eg. no notification are sent for CAPTURED payment.

    To follow payment status, you must use web services.
    """

    def get(self, request, *args, **kwargs):
        if request.user and request.user.is_superuser:
            # Authorize admins for test purpose to copy the GET params
            #  to the POST dict
            request.POST = request.GET
            return self.post(request, *args, **kwargs)
        return HttpResponse()

    def post(self, request, *args, **kwargs):
        try:
            self.handle_ipn(request)
        except PaymentError as inst:
            return HttpResponseBadRequest(inst.message)

        # todo: send message to customer

        return HttpResponse('ok')

    def handle_ipn(self, request, **kwargs):
        """
        Complete payment. Register:
            - transaction
            - source
            - payment event
        :param request: request from IpnView
        :return: None
        """

        # TODO: Avoid duplicate transaction / source / payment event

        try:
            txn = Facade().set_txn(request)
        except SystemPayError:
            return

        source_type, _ = SourceType.objects.get_or_create(name='systempay')
        trans_status = txn.value('vads_trans_status')
        payment_event = '%s-%s' % (txn.operation_type, trans_status)

        refunded = allocated = debited = D(0)

        # We force here the allocated amount to be captured since SystemPay
        # doesn't send any notification on a capture event
        # (authorised = captured)
        if txn.operation_type == SystemPayTransaction.OPERATION_TYPE_DEBIT \
                and not trans_status == CANCELLED:
            # if self.AUTHORISED in trans_status:
            # allocated = txn.amount
            # elif self.CAPTURED in trans_status:
            #     debited = txn.amount
            debited = txn.amount
        elif txn.operation_type == SystemPayTransaction.OPERATION_TYPE_CREDIT \
                and not trans_status == CANCELLED:
            # if self.AUTHORISED in trans_status:
            #     allocated = txn.amount
            # elif self.CAPTURED in trans_status:
            #     refunded = txn.amount
            refunded = txn.amount
        else:
            raise PaymentError(
                _("Unknown operation type '%(operation_type)s'")
                % {'operation_type': txn.operation_type})

        source = Source(source_type=source_type,
                        currency=txn.currency,
                        amount_allocated=allocated,
                        amount_debited=debited,
                        amount_refunded=refunded,
                        reference=txn.reference)

        order = self.create_order(
            txn.order_number, txn.trans_id, txn.trans_date
        )

        # Update order status to 'being processed'
        handler = EventHandler()
        handler.handle_order_status_change(
            order, getattr(settings, 'OSCAR_STATUS_BEING_PROCESSED', None))

        self.add_payment_source(source)
        self.add_payment_event(payment_event,
                               txn.amount, reference=txn.reference)
        self.save_payment_details(order)

        return txn

    def create_order(self, order_number, trans_id, trans_date):
        submit_txn = SystemPayTransaction.objects.get(
            order_number=order_number,
            mode=SystemPayTransaction.MODE_SUBMIT,
            trans_id=trans_id,
            trans_date=trans_date
        )

        SessionStore = import_module(settings.SESSION_ENGINE).SessionStore
        session = SessionStore(session_key=submit_txn.session_key)

        basket_id = (
            session
            .get('checkout_data', {})
            .get('submission', {})
            .get('basket_id')
        )
        basket = get_object_or_404(Basket, pk=basket_id)

        request = RequestFactory()
        request.session = session
        request.user = basket.owner or AnonymousUser()
        request.basket = basket

        self.checkout_session = CheckoutSessionData(request)

        basket.strategy = selector.strategy(
            request=request, user=basket.owner
        )
        Applicator().apply(basket, basket.owner, request)
        kwargs = self.build_submission(basket=basket, user=request.user)
        kwargs.update(kwargs.pop('order_kwargs', {}))
        del kwargs['payment_kwargs']
        try:
            order = self.place_order(
                order_number,
                **kwargs
            )
            basket.submit()

            # Send confirmation message (normally an email)
            self.send_confirmation_message(order, self.communication_type_code)

            return order
        except UnableToPlaceOrder as e:
            msg = str(e)
            logger.error("Order #%s: unable to place order - %s",
                         order_number, msg, exc_info=True)
            self.restore_frozen_basket()
            raise e
